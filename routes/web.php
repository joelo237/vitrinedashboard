<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login');
Route::get('/home', 'App\Http\Controllers\indexController@listLogFile')->name('index');
Route::get('/dashboard/form/publication', function () {
    return view('addPublication');
})->name('formRealisation');
Route::post('/storePublication', 'App\Http\Controllers\PublicationController@store')->name('storePublication');
Route::get('/dashboard/list/realisation', 'App\Http\Controllers\PublicationController@listRealisation')->name('listRealisation');

Route::get('/dashboard/list/Welcome', 'App\Http\Controllers\PublicationController@listWelcome')->name('listWelcome');
Route::get('/dashboard/list/About', 'App\Http\Controllers\PublicationController@listAbout')->name('listAbout');
Route::get('/dashboard/list/Services', 'App\Http\Controllers\PublicationController@listService')->name('listService');
Route::get('/dashboard/list/Contact_Us', 'App\Http\Controllers\PublicationController@listContact')->name('listContact');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
