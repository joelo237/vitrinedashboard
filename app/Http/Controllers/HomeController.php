<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $logFile = DB::table('LogFile')
        ->where('status',2)
        ->orderBy('id','desc')
        ->get();
        $count=count($logFile); 

        
        return view('index',['logFiles'=>$logFile, 'count'=>$count]);
        //return view('index');
    }
}
