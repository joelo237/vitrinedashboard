<?php

namespace App\Http\Controllers;
use App\Models\Realisations;
use App\Models\Welcome;
use App\Models\Services;
use App\Models\Contact;
use App\Models\About;
use App\Models\LogFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PublicationController extends Controller
{

    /*
    public function create()
    {
        return view('addEnterprise');
    }*/
    public function store(Request $request)
    {
       // dd($request);

       // $data = $request->all();

        //$data = $request->validated();
       // $data = $request;
       
//create Realisation--------------------------------------------------------------------
        if($request->type=="Realisation")
            {
                 //dd($request);
                if(isset($request->image) && $request->image != NULL)
                {
                    $business_path = "images/".md5($request->image).'.'.$request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move(public_path() . '/images' , $business_path);
       
                }
                
                $query = [
                    // 'ref' => $this->generate_ref(),
                     'titre' => $request->title1,
                     'description' => $request->description1,
                     'image'=>$business_path,
                     'status' => 2,
                    ];

                    Realisations::create($query);

                    $logquery= [
                        // 'ref' => $this->generate_ref(),
                         'title' => 'Our Realisation',
                         'description' => 'New publication in our realisation created ',
                         'status' => 2,
                        ];
                    LogFile::create($logquery);

                    
                    return redirect()->route('listRealisation');
            }
    // End Realisation -----------------------------------------------------------------

    // Create Welcome -----------------------------------------------------------------
    if($request->type=="welcome")
    {
        if(isset($request->image_slide) && $request->image_slide != NULL)
        {
            $business_path1 = "images/".md5($request->image_slide).'.'.$request->file('image_slide')->getClientOriginalExtension();
            $request->file('image_slide')->move(public_path() . '/images' ,$business_path1);

        }
        if(isset($request->image) && $request->image != NULL)
        {
            $business_path = "images/".md5($request->image).'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path() . '/images' , $business_path);

        }
        
        $query = [
            // 'ref' => $this->generate_ref(),
             'title' => $request->title1,
             'description' => $request->description1,
             'image'=>$business_path,
             'slide' => $business_path1,
             'status' => 2,
            ];

            Welcome::create($query);

            $logquery= [
                // 'ref' => $this->generate_ref(),
                 'title' => 'Welcome Publication ',
                 'description' => 'Welcome Page Created ',
                 'status' => 2,
                ];
            LogFile::create($logquery);

            
            return redirect()->route('listWelcome');
        }
// End create Welcome -----------------------------------------------------------------

// Create  About Us -----------------------------------------------------------------
if($request->type=="About")
{
    if(isset($request->image_slide) && $request->image_slide != NULL)
    {
        $business_path1 = "images/".md5($request->image_slide).'.'.$request->file('image_slide')->getClientOriginalExtension();
        $request->file('image_slide')->move(public_path() . '/images' ,$business_path1);

    }
    
    $query = [
        // 'ref' => $this->generate_ref(),
         'title1' => $request->title1,
         'title2' => $request->title2,
         'description1' => $request->description1,
         'description2' => $request->description2,
         'localisation' => $request->localisation,
         'contact' => $request->phone,
         'image'=>$business_path1,
         'status' => 2,
        ];

        About::create($query);

        $logquery= [
            // 'ref' => $this->generate_ref(),
             'title' => 'About US',
             'description' => 'About us page created ',
             'status' => 2,
            ];
        LogFile::create($logquery);

        
        return redirect()->route('listAbout');
    }
// End create About Us -----------------------------------------------------------------

// Create  Service  -----------------------------------------------------------------
if($request->type=="Services")
{
    if(isset($request->image) && $request->image != NULL)
        {
            $business_path = "images/".md5($request->image).'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path() . '/images' , $business_path);

        }
    
    $query = [
        // 'ref' => $this->generate_ref(),
         'title' => $request->title1,
         'description1' => $request->description1,
         'description2' => $request->description2,
         'image'=>$business_path,
         'status' => 2,
        ];

        Services::create($query);

        $logquery= [
            // 'ref' => $this->generate_ref(),
             'title' => 'Our Services',
             'description' => 'Service page created ',
             'status' => 2,
            ];
        LogFile::create($logquery);

        
        return redirect()->route('listService');
    }
// End create Service -----------------------------------------------------------------

// Create  Our Contact  -----------------------------------------------------------------
if($request->type=="Contact")
{ 
    $query = [
        // 'ref' => $this->generate_ref(),
         'name' => $request->title1,
         'description' => $request->description1,
         'phone' => $request->phone,
         'email'=>$request->email,
         'city'=>$request->localisation,
         'status' => 2,
        ];

        Contact::create($query);

        $logquery= [
            // 'ref' => $this->generate_ref(),
             'title' => 'Our Contact',
             'description' => 'New Contact created ',
             'status' => 2,
            ];
        LogFile::create($logquery);

        
        return redirect()->route('listContact');
    }
// End create Contact_Us -----------------------------------------------------------------


    }

// List our Realisation -----------------------------------------------------------------
    public function listRealisation()
    {
        $listRealisation = DB::table('Realisations')
        ->where('status',2)
        ->orderBy('id','desc')
        ->get();

        
        return view('listRealisation',['listRealisations'=>$listRealisation]);
    }
// End List Realisation -----------------------------------------------------------------

// List Welcome -----------------------------------------------------------------
    public function listWelcome()
    {
        $listWelcome = DB::table('Welcome')
        ->where('status',2)
        ->orderBy('id','desc')
        ->get();

        
        return view('listWelcome',['listWelcomes'=>$listWelcome]);
    }
// End List Welcome ----------------------------------------------------------------- 

// List About -----------------------------------------------------------------
public function listAbout()
{
    $listAbout = DB::table('About')
    ->where('status',2)
    ->orderBy('id','desc')
    ->get();

    
    return view('listAbout',['listAbouts'=>$listAbout]);
}
// End List About ----------------------------------------------------------------- 

// List Service -----------------------------------------------------------------
public function listService()
{
    $listAbout = DB::table('Services')
    ->where('status',2)
    ->orderBy('id','desc')
    ->get();

    
    return view('listService',['listServices'=>$listAbout]);
}
// End List Service ----------------------------------------------------------------- 

// List Contact_Us -----------------------------------------------------------------
public function listContact()
{
    $listContact = DB::table('Contact')
    ->where('status',2)
    ->orderBy('id','desc')
    ->get();

    return view('listContact',['listContacts'=>$listContact]);
}
// End List Contact_us ----------------------------------------------------------------- 
    
    
}


