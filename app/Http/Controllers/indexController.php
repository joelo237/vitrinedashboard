<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class indexController extends Controller
{
    
    public function listLogFile()
    {
        $logFile = DB::table('LogFile')
        ->where('status',2)
        ->orderBy('id','desc')
        ->get();
        $count=count($logFile); 

        
        return view('index',['logFiles'=>$logFile, 'count'=>$count]);
    }
}
