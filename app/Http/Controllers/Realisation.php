<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Realisation extends Controller
{
    /*
    public function create()
    {
        return view('addEnterprise');
    }*/

    public function store(Request $request)
    {
        $data = $request;

        $query = [
           // 'ref' => $this->generate_ref(),
            'name' => $data['name'],
            'description' => $data['description'],
            'status' => 2,
        ];

        Enterprise::create($query);
        
        return redirect()->route('dashboard');
    }

    public function list()
    {
        $listEntreprise = DB::table('enterprise')
        ->where('status',2)
        ->orderBy('id','desc')
        ->get();

        
        return view('listEnterprise',['listEntreprises'=>$listEntreprise]);
    }
}

