<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogFile extends Model
{
    use HasFactory;

    protected $table = 'logfile';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'description',                                                                          
        'status',
        'updated_at',
        'created_at'

    ];
}

