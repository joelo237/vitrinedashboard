<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;
    
    protected $table = 'about';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title1',
        'title2',
        'description1',
        'description2',
        'image',
        'localisation',
        'contact',
        'status',
        'updated_at',
        'created_at'

    ];
}
