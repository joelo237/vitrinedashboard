<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Realisations extends Model
{
    use HasFactory;

    protected $table = 'realisations';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'titre',
        'description',
        'image',
        'status',
        'updated_at',
        'created_at'

    ];
}
