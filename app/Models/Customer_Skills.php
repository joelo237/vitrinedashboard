<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer_Skills extends Model
{
    use HasFactory;
    protected $table = 'customerskills';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'description',
        'image',
        'status',
        'updated_at',
        'created_at'

    ];
}
