@extends('layout.header')

@section('main')

    <div class="pagetitle">
      <h1>All About Us Page</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">About Us </li>
          <li class="breadcrumb-item active">List</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"> About Us Page</h5>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                  <th scope="col">Image</th>
                    <th scope="col">Date</th>
                    <th scope="col">Title</th>
                    <th scope="col">Sub Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Sub Description</th>
                    <th scope="col">Localisation</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($listAbouts as $item)
                <tr>
                <td><img src="{{asset('')}}{{$item->image}}" width="40px"></td>
                <td>{{$item->created_at}}</td>
                <td>{{$item->title1}}</td>
                <td>{{$item->title2}}</td>
                <td>{{$item->description1}}</td>
                <td>{{$item->description2}}</td>
                <td>{{$item->localisation}}</td>
                <td>{{$item->contact}}</td> 
                <td> 
                  @if ($item->status == 2)                
                     <span class="badge bg-success">Active</span>
                  @endif
                </td>
                <td><a href="">update</a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>
    </main>
 @endsection