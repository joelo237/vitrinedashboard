@extends('layout.header')

@section('main')

        <div class="content">
            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-sm-12 col-xl-6">
                        <div class="bg-light rounded h-100 p-4">
                            <h6 class="mb-4">Add Publication </h6>
                            <form method="POST" action="{{ route('storePublication') }}" enctype="multipart/form-data">
                             @csrf
                            <div class="form-floating mb-3">
                                <select class="form-select" id="floatingSelect" name="type"
                                    aria-label="Floating label select example">
                                    <option value="About">About</option>
                                    <option value="Services">Services</option>
                                    <option value="Contact">Contact</option>
                                    <option value="welcome">welcome</option>
                                    <option value="Realisation">Realisation</option>
                                    <option value="Customer">Customer Skills</option>
                                    <option value="Galery">Galery</option>
                                </select>
                                <label for="floatingSelect">Select Page</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" name='title1' class="form-control" id="floatingInput"
                                    placeholder="">
                                <label for="floatingInput">title1</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" name='title2' class="form-control" id="floatingInput"
                                    placeholder="">
                                <label for="floatingInput">title2</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="text" name='localisation' class="form-control" id="floatingPassword"
                                    placeholder="">
                                <label for="floatingInput">localisation</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="email" name='email' class="form-control" id="floatingEmail"
                                    placeholder="">
                                <label for="floatingEmail">email</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="Number" name='phone' class="form-control" id="floatingEmail"
                                    placeholder="">
                                <label for="floatingNumber">phone</label>
                            </div>
                            <!--div class="form-floating mb-3">
                                <select class="form-select" id="floatingSelect"
                                    aria-label="Floating label select example">
                                    <option selected>Open this select menu</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                                <label for="floatingSelect">Works with selects</label>
                            </div-->
                            <div class="form-floating">
                                <textarea class="form-control" name='description1' placeholder="Leave a comment here"
                                    id="floatingTextarea" style="height: 150px;"></textarea>
                                <label for="floatingTextarea">Description1</label>
                            </div>
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Logo</label>
                                <input class="form-control" type="file" id="formFile" name="logo">
                            </div>
                            <div class="form-floating">
                                <textarea class="form-control" name='description2' placeholder="Leave a comment here"
                                    id="floatingTextarea" style="height: 150px;"></textarea>
                                <label for="floatingTextarea">Description2</label>
                            </div>
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Slide Image</label>
                                <input class="form-control" type="file" id="formFile" name="image_slide">
                            </div>
                           
                            <div class="mb-3">
                                <label for="formFile" class="form-label">image</label>
                                <input class="form-control" type="file" id="formFile" name="image">
                            </div>
                            <div class="mb-3">
                            <center><button type="submit" class="btn btn-primary">Enrégistrer</button></center>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection>