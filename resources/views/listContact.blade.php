@extends('layout.header')

@section('main')

    <div class="pagetitle">
      <h1>Contact Us</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Contact </li>
          <li class="breadcrumb-item active">List</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"> Contact List</h5>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                  <th scope="col">ID</th>
                    <th scope="col">Date</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Phone</th>
                    <th scope="col">email</th>
                    <th scope="col">localisation</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($listContacts as $item)
                <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->created_at}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->description}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->city}}</td>
                <td> 
                  @if ($item->status == 2)                
                     <span class="badge bg-success">Active</span>
                  @endif
                </td>
                <td><a href="">update</a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>
    </main>
 @endsection