@extends('layout.header')

@section('main')

    <div class="pagetitle">
      <h1>All Welcome Page</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Welcome </li>
          <li class="breadcrumb-item active">List</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"> Welcome Page</h5>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                  <th scope="col">Image</th>
                    <th scope="col">Date</th>
                    <th scope="col">Title</th>
                    <th scope="col">Slide Image</th>
                    <th scope="col">Description</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($listWelcomes as $item)
                <tr>
                <td><img src="{{asset('')}}{{$item->image}}" width="40px"></td>
                <td>{{$item->created_at}}</td>
                <td>{{$item->title}}</td>
                <td><img src="{{asset('')}}{{$item->slide}}" width="40px"></td> 
                <td>{{$item->description}}</td>
                <td> 
                  @if ($item->status == 2)                
                     <span class="badge bg-success">Active</span>
                  @endif
                </td>
                <td><a href="">update</a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

        
      </div>
    </section>
    </main>
 @endsection